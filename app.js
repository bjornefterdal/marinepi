var app = require('express')();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var xml2js = require('xml2js');
var sensorLib = require('node-dht-sensor');
var serialport = require('serialport');
var nmea = require('nmea');


// NMEA


var port = new serialport.SerialPort('/dev/ttyAMA0', {
                baudrate: 9600,
                parser: serialport.parsers.readline('\r\n')});
    
port.on('data', function(line) {
    
   // console.log('sats:'+nmea.parse(line).type);
     console.log(nmea.parse(line));
   // if (nmea.parse(line).type=='fix'){
      io.emit('nmeafix', nmea.parse(line));
     
     
   // };
    //console.log(nmea.parse(line));
});


app.get('/', function(req, res){
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/stylesheets/materialize.min.css', function(req, res){
  res.sendFile(__dirname + '/public/stylesheets/materialize.min.css');
});

app.get('/javascripts/materialize.min.js', function(req, res){
  res.sendFile(__dirname + '/public/javascripts/materialize.min.js');
});

app.get('/font/roboto/Roboto-Regular.woff2', function(req, res){
  res.sendFile(__dirname + '/public/font/roboto/Roboto-Regular.woff2');
});

app.get('/font/roboto/Roboto-Light.woff2', function(req, res){
  res.sendFile(__dirname + '/public/font/roboto/Roboto-Light.woff2');
});




// DHT11 Sensor

var sensor = {
    initialize: function () {
        return sensorLib.initialize(11, 4);
    }
  }

if (sensor.initialize()) {
    console.log('Sensor OK');
} else {
    console.warn('Failed to initialize sensor');
}



io.on('connection', function(socket){
  console.log('a user connected');

    // DHT11 sensor
     setInterval(function(){
        var readout = sensorLib.read();
        
        io.emit('climate', readout);

    }, 2000);


    


});




http.listen(80, function(){
  console.log('listening on *:80');
});
